const addButton = document.getElementById('addButton');

let phoneDirectory = [];

const deleteRow = async (id) => {
  const response = await fetch(`/api/contact/${id}`, { method: 'DELETE' });
  await response.text();

  fetchRows();
};

const updateContact = async ({ id, name, phone, email, address }) => {
  const response = await fetch(`/api/contact/${id}`, {
    method: 'PUT',
    body: JSON.stringify({ name, phone, email, address }),
    headers: {
          'Content-Type': 'application/json',
    },
  });
  const newContact = await response.json();

  fetchRows();
};

function editData(id, nameF, phoneF, emailF, addressF) {
  addressF.contentEditable = "true";
  addressF.focus();

  const name = nameF.innerText;
  const phone = phoneF.innerText;
  const email = emailF.innerText;

  addressF.addEventListener("keyup", function(event){
    if (event.keyCode === 13) {
      addressF.contentEditable = "false";
      var address = addressF.innerText.split('\n')[0];
      updateContact({ id, name, phone, email, address });
    }
  });
  addressF.addEventListener("focusout", function(event){
    fetchRows();
  });

}

const renderRows = () => {
  const tbody = document.getElementById('tbody');

  tbody.innerHTML = '';

  const rows = phoneDirectory.map(({
    id, name, phone, email, address,
  }) => {
    const row = document.createElement('tr');

    const nameField = document.createElement('td');
    nameField.innerText = name;
    row.appendChild(nameField);
    const phoneField = document.createElement('td');
    phoneField.innerText = phone;
    row.appendChild(phoneField);
    const emailField = document.createElement('td');
    emailField.innerText = email;
    row.appendChild(emailField);
    const addressField = document.createElement('td');
    addressField.innerText = address;
    addressField.innerText.type = "text";
    addressField.ondblclick = () => editData(id, nameField, phoneField, emailField, addressField);
    row.appendChild(addressField);
    const buttonField = document.createElement('td');
    const button = document.createElement('button');
    button.innerText = 'Remove';
    button.onclick = () => deleteRow(id);
    buttonField.appendChild(button);
    row.appendChild(buttonField);

    return row;
  });

  rows.forEach((row) => {
    tbody.appendChild(row);
  });
};

const fetchRows = async () => {
  const response = await fetch('/api/contacts');
  const phoneDirectoryArray = await response.json();

  phoneDirectory = [];
  phoneDirectory = phoneDirectoryArray;

  renderRows();
};

const addNewContact = async ({ name, phone, email, address }) => {

  const response = await fetch('/api/contact', {
    method: 'POST',
    body: JSON.stringify({ name, phone, email, address }),
    headers: {
          'Content-Type': 'application/json',
    },
  });
  const newContact = await response.json();

  phoneDirectory.push(newContact);

  fetchRows();

};

addButton.onclick = () => {

  const iName = document.getElementById('iName');
  const iPhone = document.getElementById('iPhone');
  const iEmail = document.getElementById('iEmail');
  const iAddress = document.getElementById('iAddress')

  const name = iName.value;
  const phone = iPhone.value;
  const email = iEmail.value;
  const address = iAddress.value;
  
  if(name == "" || phone == "" || email == "" || address == "" ) {
    alert("Error: Input is empty!");
    return false;
  } 

  var nameRegEx = /^[ a-zA-ZÀ-ÿ\u00f1\u00d1]*$/g;
  if(!nameRegEx.test(name)){
    alert("Error: name has invalid characters.");
    return false;
  }
  
  var phoneRegEx = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;

  if(!phoneRegEx.test(phone)){
    alert("Error: phone nummber is invalid!");
    return false;
  }

  var emailRegEx = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if(!emailRegEx.test(email)){
    alert("Error: email is invalid!");
    return false;
  }

  iName.value = '';
  iPhone.value = '';
  iEmail.value = '';
  iAddress.value = '';

  addNewContact({ name, phone, email, address });
  

};

fetchRows();
